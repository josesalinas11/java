package com.example.demo.controllers;


import com.example.demo.model.Customer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("/customers")
@RestController
public class CustomersController {

    @RequestMapping("/")
    public List<Customer> getAll(){
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer("1", "Juan", "COCA COLA"));
        customers.add(new Customer("2", "Lucas", "LA GRANJA"));
        customers.add(new Customer("3", "Pablo", "CRIA AVES"));
        customers.add(new Customer("4", "Juana", "EL ROBLE"));
        return customers;
    }

}
